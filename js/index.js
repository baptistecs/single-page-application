const config = {
  movies: {
    list: {
      fields: { year: 'Year', title: 'Title', score: 'Score' },
    },
  },
};

const listMovies = ({ movies }) => {
  const list = document.getElementById('movie-list');
  if (list && movies) {
    const {
      movies: {
        list: { fields },
      },
    } = config;
    addListHead(list, fields, Object.keys(fields), 'list-head movie-head');
    movies.forEach((movie) => addListItem(list, movie, Object.keys(fields), 'list-item movie-item'));
  }
};

const addListHead = (list, fields, fieldKeys, className = 'list-head') => {
  const div = document.createElement('div');
  div.className = className;
  let content = '';
  fieldKeys.forEach((fieldKey) => {
    content += `
      <div class="list-item-${fieldKey}">${typeof fields[fieldKey] === 'undefined' ? '' : fields[fieldKey]}</div>`;
  });
  div.innerHTML = content;
  list.appendChild(div);
};

const addListItem = (list, item, fieldKeys, className = 'list-item') => {
  const div = document.createElement('div');
  div.className = className;
  let content = '';
  fieldKeys.forEach((fieldKey) => {
    content += `
      <div class="list-item-${fieldKey}">${typeof item[fieldKey] === 'undefined' ? '' : item[fieldKey]}</div>`;
  });
  div.innerHTML = content;
  list.appendChild(div);
};

const getPageData = (pageName, defaultPageName = 'home') => {
  const pages = {
    404: { content: 'This page doesn´t exist.' },
    home: { content: 'This is the Home page. Welcome to my site.' },
    about: { content: 'This page will describe what my site is about' },
    contact: { content: 'Contact me on this page if you have any questions' },
    movies: {
      content: `
        <h1>Movies</h1>
        <div id="movie-list" class="movie-list"></div>`,
      movies: [
        { title: 'Movie A', score: 4.9, year: 2012 },
        { title: 'Movie B', score: 2.4, year: 2008 },
        { title: 'Movie C Movie C Movie C Movie C Movie C Movie C Movie C Movie C', score: 3.8, year: 2021 },
      ],
      callback: listMovies,
    },
  };

  if (pageName === '') {
    pageName = defaultPageName;
  }

  return typeof pages[pageName] === 'undefined' ? pages[404] : pages[pageName];
};

const loadPage = () => {
  const contentDiv = document.getElementById('app');
  const pageName = location.hash.substr(1);
  const pageData = getPageData(pageName);
  const { content = '', callback } = pageData;
  contentDiv.innerHTML = content;
  callback && callback(pageData);
};

window.addEventListener('hashchange', loadPage);

loadPage();
